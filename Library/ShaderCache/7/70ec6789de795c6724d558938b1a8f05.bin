�A                       +  #ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_LightColor[8];
uniform 	vec4 unity_LightPosition[8];
uniform 	mediump vec4 unity_LightAtten[8];
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	mediump vec4 glstate_lightmodel_ambient;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _TintColor;
uniform 	float _DirectionalLightMultiplier;
uniform 	float _PointSpotLightMultiplier;
uniform 	mediump vec3 _EmissiveColor;
uniform 	mediump float _AmbientLightMultiplier;
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in mediump vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
in highp vec3 in_NORMAL0;
out mediump vec2 vs_TEXCOORD0;
out mediump vec4 vs_COLOR0;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
bool u_xlatb12;
void main()
{
    u_xlat0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD0.xy = u_xlat0.xy;
    u_xlat0.xyz = (-in_POSITION0.xyz) + unity_LightPosition[0].xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat1.x = inversesqrt(u_xlat12);
    u_xlat12 = u_xlat12 * unity_LightAtten[0].z + 1.0;
    u_xlat12 = float(1.0) / u_xlat12;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx;
    u_xlat0.x = dot(in_NORMAL0.xyz, u_xlat0.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.xyz = u_xlat0.xxx * unity_LightColor[0].xyz;
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat16_2.xyz = glstate_lightmodel_ambient.xyz * vec3(vec3(_AmbientLightMultiplier, _AmbientLightMultiplier, _AmbientLightMultiplier));
    u_xlat16_2.xyz = u_xlat16_2.xyz * vec3(2.0, 2.0, 2.0) + _EmissiveColor.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_PointSpotLightMultiplier, _PointSpotLightMultiplier, _PointSpotLightMultiplier)) + u_xlat16_2.xyz;
    u_xlat12 = dot(unity_LightPosition[0], unity_LightPosition[0]);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * unity_LightPosition[0].xyz;
    u_xlat12 = dot(in_NORMAL0.xyz, (-u_xlat1.xyz));
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat16_3.xyz = vec3(u_xlat12) * unity_LightColor[0].xyz;
    u_xlat1.xyz = u_xlat16_3.xyz * vec3(_DirectionalLightMultiplier) + u_xlat16_2.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(unity_LightPosition[0].w==0.0);
#else
    u_xlatb12 = unity_LightPosition[0].w==0.0;
#endif
    u_xlat16_2.xyz = (bool(u_xlatb12)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat0.xyz = (-in_POSITION0.xyz) + unity_LightPosition[1].xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat1.x = inversesqrt(u_xlat12);
    u_xlat12 = u_xlat12 * unity_LightAtten[1].z + 1.0;
    u_xlat12 = float(1.0) / u_xlat12;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx;
    u_xlat0.x = dot(in_NORMAL0.xyz, u_xlat0.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.xyz = u_xlat0.xxx * unity_LightColor[1].xyz;
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_PointSpotLightMultiplier, _PointSpotLightMultiplier, _PointSpotLightMultiplier)) + u_xlat16_2.xyz;
    u_xlat12 = dot(unity_LightPosition[1], unity_LightPosition[1]);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * unity_LightPosition[1].xyz;
    u_xlat12 = dot(in_NORMAL0.xyz, (-u_xlat1.xyz));
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat16_3.xyz = vec3(u_xlat12) * unity_LightColor[1].xyz;
    u_xlat1.xyz = u_xlat16_3.xyz * vec3(_DirectionalLightMultiplier) + u_xlat16_2.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(unity_LightPosition[1].w==0.0);
#else
    u_xlatb12 = unity_LightPosition[1].w==0.0;
#endif
    u_xlat16_2.xyz = (bool(u_xlatb12)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat0.xyz = (-in_POSITION0.xyz) + unity_LightPosition[2].xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat1.x = inversesqrt(u_xlat12);
    u_xlat12 = u_xlat12 * unity_LightAtten[2].z + 1.0;
    u_xlat12 = float(1.0) / u_xlat12;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx;
    u_xlat0.x = dot(in_NORMAL0.xyz, u_xlat0.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.xyz = u_xlat0.xxx * unity_LightColor[2].xyz;
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_PointSpotLightMultiplier, _PointSpotLightMultiplier, _PointSpotLightMultiplier)) + u_xlat16_2.xyz;
    u_xlat12 = dot(unity_LightPosition[2], unity_LightPosition[2]);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * unity_LightPosition[2].xyz;
    u_xlat12 = dot(in_NORMAL0.xyz, (-u_xlat1.xyz));
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat16_3.xyz = vec3(u_xlat12) * unity_LightColor[2].xyz;
    u_xlat1.xyz = u_xlat16_3.xyz * vec3(_DirectionalLightMultiplier) + u_xlat16_2.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(unity_LightPosition[2].w==0.0);
#else
    u_xlatb12 = unity_LightPosition[2].w==0.0;
#endif
    u_xlat16_2.xyz = (bool(u_xlatb12)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat0.xyz = (-in_POSITION0.xyz) + unity_LightPosition[3].xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat1.x = inversesqrt(u_xlat12);
    u_xlat12 = u_xlat12 * unity_LightAtten[3].z + 1.0;
    u_xlat12 = float(1.0) / u_xlat12;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx;
    u_xlat0.x = dot(in_NORMAL0.xyz, u_xlat0.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.xyz = u_xlat0.xxx * unity_LightColor[3].xyz;
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_PointSpotLightMultiplier, _PointSpotLightMultiplier, _PointSpotLightMultiplier)) + u_xlat16_2.xyz;
    u_xlat12 = dot(unity_LightPosition[3], unity_LightPosition[3]);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * unity_LightPosition[3].xyz;
    u_xlat12 = dot(in_NORMAL0.xyz, (-u_xlat1.xyz));
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat16_3.xyz = vec3(u_xlat12) * unity_LightColor[3].xyz;
    u_xlat1.xyz = u_xlat16_3.xyz * vec3(_DirectionalLightMultiplier) + u_xlat16_2.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(unity_LightPosition[3].w==0.0);
#else
    u_xlatb12 = unity_LightPosition[3].w==0.0;
#endif
    u_xlat16_0.xyz = (bool(u_xlatb12)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat16_0.w = 1.0;
    u_xlat16_0 = u_xlat16_0 * in_COLOR0;
    vs_COLOR0 = u_xlat16_0 * _TintColor;
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in mediump vec2 vs_TEXCOORD0;
in mediump vec4 vs_COLOR0;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec4 u_xlat16_0;
lowp vec4 u_xlat10_0;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_0 = u_xlat10_0 * vs_COLOR0;
    SV_Target0 = u_xlat16_0;
    return;
}

#endif
                               