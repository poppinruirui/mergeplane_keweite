�A                         SOFTPARTICLES_ON   PERSPECTIVE    ORTHOGRAPHIC_XZ    INTENSITY_FLICKER     #ifdef VERTEX
#version 300 es

uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _LightningTime;
uniform 	float _JitterMultiplier;
uniform 	float _Turbulence;
uniform 	vec4 _TurbulenceVelocity;
uniform 	vec4 _IntensityFlicker;
uniform 	mediump vec4 _TintColor;
uniform lowp sampler2D _IntensityFlickerTexture;
in highp vec4 in_POSITION0;
in highp vec4 in_TANGENT0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec2 vs_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
vec3 u_xlat3;
bool u_xlatb3;
float u_xlat6;
vec2 u_xlat7;
float u_xlat9;
void main()
{
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    u_xlat0.x = _LightningTime.y * _IntensityFlicker.z + in_TEXCOORD1.x;
    u_xlat0.y = in_TEXCOORD1.x;
    u_xlat0.x = textureLod(_IntensityFlickerTexture, u_xlat0.xy, 0.0).w;
    u_xlat0.x = u_xlat0.x + _IntensityFlicker.w;
    u_xlat3.x = (-_IntensityFlicker.x) + _IntensityFlicker.y;
    u_xlat0.x = u_xlat0.x * u_xlat3.x + _IntensityFlicker.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3 = !!(_LightningTime.y<in_TEXCOORD1.y);
#else
    u_xlatb3 = _LightningTime.y<in_TEXCOORD1.y;
#endif
    u_xlat6 = u_xlatb3 ? 1.0 : float(0.0);
    u_xlat1.xyz = vec3((-in_TEXCOORD1.x) + in_TEXCOORD1.w, (-in_TEXCOORD1.x) + in_TEXCOORD1.y, (-in_TEXCOORD1.z) + in_TEXCOORD1.w);
    u_xlat9 = max(u_xlat1.z, 9.99999975e-06);
    u_xlat7.xy = (-in_TEXCOORD1.xz) + _LightningTime.yy;
    u_xlat9 = u_xlat7.y / u_xlat9;
#ifdef UNITY_ADRENO_ES3
    u_xlat9 = min(max(u_xlat9, 0.0), 1.0);
#else
    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
#endif
    u_xlat9 = (-u_xlat9) + 1.0;
    u_xlat3.x = (u_xlatb3) ? 0.0 : u_xlat9;
    u_xlat9 = u_xlat1.y + 9.99999975e-06;
    u_xlat1.x = u_xlat7.x / u_xlat1.x;
    u_xlat9 = max(u_xlat9, 9.99999975e-06);
    u_xlat9 = u_xlat7.x / u_xlat9;
#ifdef UNITY_ADRENO_ES3
    u_xlat9 = min(max(u_xlat9, 0.0), 1.0);
#else
    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
#endif
    u_xlat3.x = u_xlat6 * u_xlat9 + u_xlat3.x;
    u_xlat0.x = u_xlat0.x * u_xlat3.x;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0 = u_xlat0.xxxx * _TintColor.wxyz;
    u_xlat2.xyz = vec3(u_xlat0.y * in_COLOR0.x, u_xlat0.z * in_COLOR0.y, u_xlat0.w * in_COLOR0.z);
    u_xlat0.x = u_xlat0.x * in_COLOR0.w;
    u_xlat2.w = u_xlat0.x * 10.0;
    vs_COLOR0 = u_xlat2;
    u_xlat0.x = max(abs(in_TANGENT0.w), 0.5);
    u_xlat0.x = _Turbulence / u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat1.x;
    u_xlat3.x = dot(in_TANGENT0.xyz, in_TANGENT0.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * in_TANGENT0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat3.xyz;
    u_xlat0.xyz = u_xlat1.xxx * _TurbulenceVelocity.xyz + u_xlat0.xyz;
    u_xlat1.xyz = (-in_POSITION0.yzx) + _WorldSpaceCameraPos.yzx;
    u_xlat2.xyz = u_xlat1.xyz * in_TANGENT0.zxy;
    u_xlat1.xyz = in_TANGENT0.yzx * u_xlat1.yzx + (-u_xlat2.xyz);
    u_xlat9 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat1.xyz = vec3(u_xlat9) * u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz * in_TANGENT0.www;
    u_xlat2.xyz = in_POSITION0.xyz * _LightningTime.xyz;
    u_xlat9 = dot(u_xlat2.xyz, vec3(12.9898005, 78.2330017, 45.5432014));
    u_xlat9 = sin(u_xlat9);
    u_xlat9 = u_xlat9 * 43758.5469;
    u_xlat9 = fract(u_xlat9);
    u_xlat9 = u_xlat9 * _JitterMultiplier + 1.0;
    u_xlat1.xyz = u_xlat1.xyz * vec3(u_xlat9) + in_POSITION0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + u_xlat1.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD1.w = u_xlat0.w;
    vs_TEXCOORD1.xy = u_xlat1.zz + u_xlat1.xw;
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat3.x = u_xlat0.y * hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[0].z * u_xlat0.x + u_xlat3.x;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[2].z * u_xlat0.z + u_xlat0.x;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[3].z * u_xlat0.w + u_xlat0.x;
    vs_TEXCOORD1.z = (-u_xlat0.x);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec4 _ZBufferParams;
uniform 	float _InvFade;
uniform highp sampler2D _CameraDepthTexture;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in mediump vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec2 u_xlat0;
lowp vec4 u_xlat10_1;
void main()
{
    u_xlat0.xy = vs_TEXCOORD1.xy / vs_TEXCOORD1.ww;
    u_xlat0.x = texture(_CameraDepthTexture, u_xlat0.xy).x;
    u_xlat0.x = _ZBufferParams.z * u_xlat0.x + _ZBufferParams.w;
    u_xlat0.x = float(1.0) / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + (-vs_TEXCOORD1.z);
    u_xlat0.x = u_xlat0.x * _InvFade;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * vs_COLOR0.w;
    u_xlat10_1 = texture(_MainTex, vs_TEXCOORD0.xy);
    SV_Target0.w = u_xlat0.x * u_xlat10_1.w;
    SV_Target0.xyz = u_xlat10_1.xyz * vs_COLOR0.xyz;
    return;
}

#endif
   =                              