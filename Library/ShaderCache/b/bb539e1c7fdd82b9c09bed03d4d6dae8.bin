�A                         INTENSITY_FLICKER   X  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

struct VGlobals_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
    float4 _LightningTime;
    float _JitterMultiplier;
    float _Turbulence;
    float4 _TurbulenceVelocity;
    float4 _IntensityFlicker;
    half4 _GlowTintColor;
};

struct Mtl_VertexIn
{
    float4 POSITION0 [[ attribute(0) ]] ;
    float4 TANGENT0 [[ attribute(1) ]] ;
    float4 COLOR0 [[ attribute(2) ]] ;
    float3 NORMAL0 [[ attribute(3) ]] ;
    float4 TEXCOORD0 [[ attribute(4) ]] ;
    float4 TEXCOORD1 [[ attribute(5) ]] ;
};

struct Mtl_VertexOut
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]];
    half4 COLOR0 [[ user(COLOR0) ]];
    float4 mtl_Position [[ position ]];
};

vertex Mtl_VertexOut xlatMtlMain(
    constant VGlobals_Type& VGlobals [[ buffer(0) ]],
    sampler sampler_IntensityFlickerTexture [[ sampler (0) ]],
    texture2d<half, access::sample > _IntensityFlickerTexture [[ texture(0) ]] ,
    Mtl_VertexIn input [[ stage_in ]])
{
    Mtl_VertexOut output;
    float4 u_xlat0;
    float4 u_xlat1;
    float4 u_xlat2;
    float u_xlat3;
    bool u_xlatb3;
    float u_xlat6;
    float2 u_xlat7;
    float u_xlat9;
    float u_xlat10;
    output.TEXCOORD0.xy = input.TEXCOORD0.xy;
    u_xlat0.x = fma(VGlobals._LightningTime.y, VGlobals._IntensityFlicker.z, input.TEXCOORD1.x);
    u_xlat0.y = input.TEXCOORD1.x;
    u_xlat0.x = float(_IntensityFlickerTexture.sample(sampler_IntensityFlickerTexture, u_xlat0.xy, level(0.0)).w);
    u_xlat0.x = u_xlat0.x + VGlobals._IntensityFlicker.w;
    u_xlat3 = (-VGlobals._IntensityFlicker.x) + VGlobals._IntensityFlicker.y;
    u_xlat0.x = fma(u_xlat0.x, u_xlat3, VGlobals._IntensityFlicker.x);
    u_xlatb3 = VGlobals._LightningTime.y<input.TEXCOORD1.y;
    u_xlat6 = u_xlatb3 ? 1.0 : float(0.0);
    u_xlat1.xyz = (-input.TEXCOORD1.xxz) + input.TEXCOORD1.wyw;
    u_xlat9 = max(u_xlat1.z, 9.99999975e-06);
    u_xlat7.xy = (-input.TEXCOORD1.xz) + VGlobals._LightningTime.yy;
    u_xlat9 = u_xlat7.y / u_xlat9;
    u_xlat9 = clamp(u_xlat9, 0.0f, 1.0f);
    u_xlat9 = (-u_xlat9) + 1.0;
    u_xlat3 = (u_xlatb3) ? 0.0 : u_xlat9;
    u_xlat9 = u_xlat1.y + 9.99999975e-06;
    u_xlat1.x = u_xlat7.x / u_xlat1.x;
    u_xlat9 = max(u_xlat9, 9.99999975e-06);
    u_xlat9 = u_xlat7.x / u_xlat9;
    u_xlat9 = clamp(u_xlat9, 0.0f, 1.0f);
    u_xlat3 = fma(u_xlat6, u_xlat9, u_xlat3);
    u_xlat0.x = u_xlat0.x * u_xlat3;
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat0 = u_xlat0.xxxx * float4(VGlobals._GlowTintColor.wxyz);
    u_xlat2.xyz = u_xlat0.yzw * input.COLOR0.xyz;
    u_xlat2.w = u_xlat0.x * input.TEXCOORD0.w;
    output.COLOR0 = half4(u_xlat2);
    u_xlat0.x = dot(input.TANGENT0.xz, input.TANGENT0.xz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xy = u_xlat0.xx * input.TANGENT0.xz;
    u_xlat0.xy = fma(u_xlat1.xx, VGlobals._TurbulenceVelocity.xz, u_xlat0.xy);
    u_xlat6 = max(abs(input.TANGENT0.w), 0.5);
    u_xlat6 = VGlobals._Turbulence / u_xlat6;
    u_xlat6 = u_xlat6 * u_xlat1.x;
    u_xlat0.xz = float2(u_xlat6) * u_xlat0.xy;
    u_xlat1.xyz = input.POSITION0.xyz * VGlobals._LightningTime.xyz;
    u_xlat9 = dot(u_xlat1.xyz, float3(12.9898005, 78.2330017, 45.5432014));
    u_xlat9 = sin(u_xlat9);
    u_xlat9 = u_xlat9 * 43758.5469;
    u_xlat9 = fract(u_xlat9);
    u_xlat9 = u_xlat9 * VGlobals._JitterMultiplier;
    u_xlat9 = fma(u_xlat9, 0.0500000007, 1.0);
    u_xlat1.x = dot(input.NORMAL0.xz, input.NORMAL0.xz);
    u_xlat1.x = rsqrt(u_xlat1.x);
    u_xlat1.xy = u_xlat1.xx * input.NORMAL0.xz;
    u_xlat7.x = input.TEXCOORD0.x + -0.5;
    u_xlat7.x = u_xlat7.x + u_xlat7.x;
    u_xlat1.xy = u_xlat7.xx * u_xlat1.xy;
    u_xlat7.x = abs(input.TANGENT0.w) + abs(input.TANGENT0.w);
    u_xlat7.x = u_xlat7.x * input.TEXCOORD0.z;
    u_xlat1.xy = u_xlat7.xx * u_xlat1.xy;
    u_xlat1.xy = fma(u_xlat1.xy, float2(1.5, 1.5), input.POSITION0.xz);
    u_xlat2.xy = input.NORMAL0.zx * float2(-1.0, 1.0);
    u_xlat10 = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat10 = rsqrt(u_xlat10);
    u_xlat2.xy = float2(u_xlat10) * u_xlat2.xy;
    u_xlat7.xy = u_xlat7.xx * u_xlat2.xy;
    u_xlat2.x = input.TANGENT0.w / abs(input.TANGENT0.w);
    u_xlat7.xy = u_xlat7.xy * u_xlat2.xx;
    u_xlat1.xz = fma(u_xlat7.xy, float2(u_xlat9), u_xlat1.xy);
    u_xlat1.y = input.POSITION0.y;
    u_xlat0.y = 0.0;
    u_xlat0.xyz = u_xlat0.xyz + u_xlat1.xyz;
    u_xlat1 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[0], u_xlat0.xxxx, u_xlat1);
    u_xlat0 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[2], u_xlat0.zzzz, u_xlat1);
    u_xlat0 = u_xlat0 + VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[0], u_xlat0.xxxx, u_xlat1);
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[2], u_xlat0.zzzz, u_xlat1);
    output.mtl_Position = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[3], u_xlat0.wwww, u_xlat1);
    return output;
}
?                                                                 VGlobals�         _LightningTime                    �      _JitterMultiplier                     �      _Turbulence                   �      _TurbulenceVelocity                   �      _IntensityFlicker                     �      _GlowTintColor                   �      unity_ObjectToWorld                         unity_MatrixVP                   @             _IntensityFlickerTexture                  VGlobals           