�A                         SOFTPARTICLES_ON   ORTHOGRAPHIC_XY `  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

struct VGlobals_Type
{
    float4 _ProjectionParams;
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
    float4 _LightningTime;
    float _JitterMultiplier;
    float _Turbulence;
    float4 _TurbulenceVelocity;
    half4 _TintColor;
};

struct Mtl_VertexIn
{
    float4 POSITION0 [[ attribute(0) ]] ;
    float4 TANGENT0 [[ attribute(1) ]] ;
    float4 COLOR0 [[ attribute(2) ]] ;
    float4 TEXCOORD0 [[ attribute(3) ]] ;
    float4 TEXCOORD1 [[ attribute(4) ]] ;
};

struct Mtl_VertexOut
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]];
    half4 COLOR0 [[ user(COLOR0) ]];
    float4 mtl_Position [[ position ]];
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]];
};

vertex Mtl_VertexOut xlatMtlMain(
    constant VGlobals_Type& VGlobals [[ buffer(0) ]],
    Mtl_VertexIn input [[ stage_in ]])
{
    Mtl_VertexOut output;
    float4 u_xlat0;
    bool u_xlatb0;
    float4 u_xlat1;
    float4 u_xlat2;
    float2 u_xlat3;
    float u_xlat6;
    float2 u_xlat7;
    float u_xlat9;
    output.TEXCOORD0.xy = input.TEXCOORD0.xy;
    u_xlatb0 = VGlobals._LightningTime.y<input.TEXCOORD1.y;
    u_xlat3.x = u_xlatb0 ? 1.0 : float(0.0);
    u_xlat1.xyz = (-input.TEXCOORD1.xxz) + input.TEXCOORD1.wyw;
    u_xlat6 = max(u_xlat1.z, 9.99999975e-06);
    u_xlat7.xy = (-input.TEXCOORD1.xz) + VGlobals._LightningTime.yy;
    u_xlat6 = u_xlat7.y / u_xlat6;
    u_xlat6 = clamp(u_xlat6, 0.0f, 1.0f);
    u_xlat6 = (-u_xlat6) + 1.0;
    u_xlat0.x = (u_xlatb0) ? 0.0 : u_xlat6;
    u_xlat6 = u_xlat1.y + 9.99999975e-06;
    u_xlat9 = u_xlat7.x / u_xlat1.x;
    u_xlat6 = max(u_xlat6, 9.99999975e-06);
    u_xlat6 = u_xlat7.x / u_xlat6;
    u_xlat6 = clamp(u_xlat6, 0.0f, 1.0f);
    u_xlat0.x = fma(u_xlat3.x, u_xlat6, u_xlat0.x);
    u_xlat0.x = min(u_xlat0.x, 1.0);
    u_xlat1 = u_xlat0.xxxx * float4(VGlobals._TintColor.wxyz);
    u_xlat2.xyz = u_xlat1.yzw * input.COLOR0.xyz;
    u_xlat0.x = u_xlat1.x * input.COLOR0.w;
    u_xlat2.w = u_xlat0.x * 10.0;
    output.COLOR0 = half4(u_xlat2);
    u_xlat0.x = max(abs(input.TANGENT0.w), 0.5);
    u_xlat0.x = VGlobals._Turbulence / u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat9;
    u_xlat3.x = dot(input.TANGENT0, input.TANGENT0);
    u_xlat3.x = rsqrt(u_xlat3.x);
    u_xlat3.xy = u_xlat3.xx * input.TANGENT0.xy;
    u_xlat0.xy = u_xlat0.xx * u_xlat3.xy;
    u_xlat0.xy = fma(float2(u_xlat9), VGlobals._TurbulenceVelocity.xy, u_xlat0.xy);
    u_xlat1.xyz = input.POSITION0.xyz * VGlobals._LightningTime.xyz;
    u_xlat9 = dot(u_xlat1.xyz, float3(12.9898005, 78.2330017, 45.5432014));
    u_xlat9 = sin(u_xlat9);
    u_xlat9 = u_xlat9 * 43758.5469;
    u_xlat9 = fract(u_xlat9);
    u_xlat9 = fma(u_xlat9, VGlobals._JitterMultiplier, 1.0);
    u_xlat1.xy = input.TANGENT0.yx * float2(-1.0, 1.0);
    u_xlat7.x = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat7.x = rsqrt(u_xlat7.x);
    u_xlat1.xy = u_xlat7.xx * u_xlat1.xy;
    u_xlat1.xy = u_xlat1.xy * input.TANGENT0.ww;
    u_xlat1.xy = float2(u_xlat9) * u_xlat1.xy;
    u_xlat1.z = 0.0;
    u_xlat1.xyz = u_xlat1.xyz + input.POSITION0.xyz;
    u_xlat0.z = 0.0;
    u_xlat0.xyz = u_xlat0.xyz + u_xlat1.xyz;
    u_xlat1 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[0], u_xlat0.xxxx, u_xlat1);
    u_xlat0 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[2], u_xlat0.zzzz, u_xlat1);
    u_xlat0 = u_xlat0 + VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[0], u_xlat0.xxxx, u_xlat1);
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[2], u_xlat0.zzzz, u_xlat1);
    u_xlat0 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[3], u_xlat0.wwww, u_xlat1);
    output.mtl_Position = u_xlat0;
    u_xlat0.y = u_xlat0.y * VGlobals._ProjectionParams.x;
    u_xlat1.xzw = u_xlat0.xwy * float3(0.5, 0.5, 0.5);
    output.TEXCOORD1.w = u_xlat0.w;
    output.TEXCOORD1.xy = u_xlat1.zz + u_xlat1.xw;
    u_xlat0 = input.POSITION0.yyyy * VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[0], input.POSITION0.xxxx, u_xlat0);
    u_xlat0 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[2], input.POSITION0.zzzz, u_xlat0);
    u_xlat0 = u_xlat0 + VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat3.x = u_xlat0.y * VGlobals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat0.x = fma(VGlobals.hlslcc_mtx4x4unity_MatrixV[0].z, u_xlat0.x, u_xlat3.x);
    u_xlat0.x = fma(VGlobals.hlslcc_mtx4x4unity_MatrixV[2].z, u_xlat0.z, u_xlat0.x);
    u_xlat0.x = fma(VGlobals.hlslcc_mtx4x4unity_MatrixV[3].z, u_xlat0.w, u_xlat0.x);
    output.TEXCOORD1.z = (-u_xlat0.x);
    return output;
}
=                                                           VGlobals  	      _ProjectionParams                            _LightningTime                    �      _JitterMultiplier                     �      _Turbulence                   �      _TurbulenceVelocity                   �   
   _TintColor                         unity_ObjectToWorld                        unity_MatrixV                    P      unity_MatrixVP                   �             VGlobals           