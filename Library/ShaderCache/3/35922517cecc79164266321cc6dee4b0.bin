�A                         PERSPECTIVE �  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

struct VGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
    float4 _LightningTime;
    float _JitterMultiplier;
    float _Turbulence;
    float4 _TurbulenceVelocity;
    half4 _GlowTintColor;
};

struct Mtl_VertexIn
{
    float4 POSITION0 [[ attribute(0) ]] ;
    float4 TANGENT0 [[ attribute(1) ]] ;
    float4 COLOR0 [[ attribute(2) ]] ;
    float3 NORMAL0 [[ attribute(3) ]] ;
    float4 TEXCOORD0 [[ attribute(4) ]] ;
    float4 TEXCOORD1 [[ attribute(5) ]] ;
};

struct Mtl_VertexOut
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]];
    half4 COLOR0 [[ user(COLOR0) ]];
    float4 mtl_Position [[ position ]];
};

vertex Mtl_VertexOut xlatMtlMain(
    constant VGlobals_Type& VGlobals [[ buffer(0) ]],
    Mtl_VertexIn input [[ stage_in ]])
{
    Mtl_VertexOut output;
    float4 u_xlat0;
    bool u_xlatb0;
    float4 u_xlat1;
    float4 u_xlat2;
    float3 u_xlat3;
    float u_xlat4;
    float u_xlat8;
    float2 u_xlat9;
    float u_xlat12;
    float u_xlat13;
    float u_xlat14;
    output.TEXCOORD0.xy = input.TEXCOORD0.xy;
    u_xlatb0 = VGlobals._LightningTime.y<input.TEXCOORD1.y;
    u_xlat4 = u_xlatb0 ? 1.0 : float(0.0);
    u_xlat1.xyz = (-input.TEXCOORD1.xxz) + input.TEXCOORD1.wyw;
    u_xlat8 = max(u_xlat1.z, 9.99999975e-06);
    u_xlat9.xy = (-input.TEXCOORD1.xz) + VGlobals._LightningTime.yy;
    u_xlat8 = u_xlat9.y / u_xlat8;
    u_xlat8 = clamp(u_xlat8, 0.0f, 1.0f);
    u_xlat8 = (-u_xlat8) + 1.0;
    u_xlat0.x = (u_xlatb0) ? 0.0 : u_xlat8;
    u_xlat8 = u_xlat1.y + 9.99999975e-06;
    u_xlat12 = u_xlat9.x / u_xlat1.x;
    u_xlat8 = max(u_xlat8, 9.99999975e-06);
    u_xlat8 = u_xlat9.x / u_xlat8;
    u_xlat8 = clamp(u_xlat8, 0.0f, 1.0f);
    u_xlat0.x = fma(u_xlat4, u_xlat8, u_xlat0.x);
    u_xlat0.x = min(u_xlat0.x, 1.0);
    u_xlat1 = u_xlat0.xxxx * float4(VGlobals._GlowTintColor.wxyz);
    u_xlat2.xyz = u_xlat1.yzw * input.COLOR0.xyz;
    u_xlat2.w = u_xlat1.x * input.TEXCOORD0.w;
    output.COLOR0 = half4(u_xlat2);
    u_xlat0.x = max(abs(input.TANGENT0.w), 0.5);
    u_xlat0.x = VGlobals._Turbulence / u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat12;
    u_xlat4 = dot(input.TANGENT0.xyz, input.TANGENT0.xyz);
    u_xlat4 = rsqrt(u_xlat4);
    u_xlat1.xyz = float3(u_xlat4) * input.TANGENT0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.xyz = fma(float3(u_xlat12), VGlobals._TurbulenceVelocity.xyz, u_xlat0.xyz);
    u_xlat1.xyz = input.POSITION0.xyz * VGlobals._LightningTime.xyz;
    u_xlat12 = dot(u_xlat1.xyz, float3(12.9898005, 78.2330017, 45.5432014));
    u_xlat12 = sin(u_xlat12);
    u_xlat12 = u_xlat12 * 43758.5469;
    u_xlat12 = fract(u_xlat12);
    u_xlat12 = u_xlat12 * VGlobals._JitterMultiplier;
    u_xlat12 = fma(u_xlat12, 0.0500000007, 1.0);
    u_xlat1.xyz = (-input.POSITION0.yzx) + VGlobals._WorldSpaceCameraPos.xyzx.yzx;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = rsqrt(u_xlat13);
    u_xlat1.xyz = float3(u_xlat13) * u_xlat1.xyz;
    u_xlat13 = dot(input.NORMAL0.xyz, input.NORMAL0.xyz);
    u_xlat13 = rsqrt(u_xlat13);
    u_xlat2.xyz = float3(u_xlat13) * input.NORMAL0.zxy;
    u_xlat3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat1.xyz = fma(u_xlat2.zxy, u_xlat1.yzx, (-u_xlat3.xyz));
    u_xlat13 = abs(input.TANGENT0.w) + abs(input.TANGENT0.w);
    u_xlat13 = u_xlat13 * input.TEXCOORD0.z;
    u_xlat1.xyz = float3(u_xlat13) * u_xlat1.xyz;
    u_xlat14 = input.TANGENT0.w / abs(input.TANGENT0.w);
    u_xlat1.xyz = u_xlat1.xyz * float3(u_xlat14);
    u_xlat14 = input.TEXCOORD0.x + -0.5;
    u_xlat14 = u_xlat14 + u_xlat14;
    u_xlat2.xyz = float3(u_xlat14) * u_xlat2.yzx;
    u_xlat2.xyz = float3(u_xlat13) * u_xlat2.xyz;
    u_xlat2.xyz = fma(u_xlat2.xyz, float3(1.5, 1.5, 1.5), input.POSITION0.xyz);
    u_xlat1.xyz = fma(u_xlat1.xyz, float3(u_xlat12), u_xlat2.xyz);
    u_xlat0.xyz = u_xlat0.xyz + u_xlat1.xyz;
    u_xlat1 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[0], u_xlat0.xxxx, u_xlat1);
    u_xlat0 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[2], u_xlat0.zzzz, u_xlat1);
    u_xlat0 = u_xlat0 + VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[0], u_xlat0.xxxx, u_xlat1);
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[2], u_xlat0.zzzz, u_xlat1);
    output.mtl_Position = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[3], u_xlat0.wwww, u_xlat1);
    return output;
}
?                                                                 VGlobals�         _WorldSpaceCameraPos                         _LightningTime                    �      _JitterMultiplier                     �      _Turbulence                   �      _TurbulenceVelocity                   �      _GlowTintColor                   �      unity_ObjectToWorld                        unity_MatrixVP                   P             VGlobals           