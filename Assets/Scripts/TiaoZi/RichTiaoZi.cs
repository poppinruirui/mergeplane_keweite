﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RichTiaoZi : MonoBehaviour {

    public SpriteRenderer _srIcon;
    public TextMesh _txtValue;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Color colorTemp = new Color();
        

    int m_nStatus = 0; // 1 - 一边放大一边上升  2 - 一边上升一边渐隐

    public float m_fInitScale = 1f;
    public float m_fMaxScale = 1.3f;
    public float m_fFirstSegDis = 1f;
    public float m_fFirstSegTime = 0.5f;
    public float m_fSecSegDis = 1f;
    public float m_fSecSegTime = 0.5f;

    float m_fScaleSpeed = 0f;
    float m_fFadeSpeed = 0f;
    float m_fRaiseAccelerate = 0f;
    float m_fRaiseSpeed = 0f;

    float m_fAlpha = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        Loop();
    }

    public void Begin()
    {
        float s = m_fFirstSegDis + m_fSecSegDis;
        float t = m_fFirstSegTime + m_fSecSegTime;
        m_fRaiseAccelerate = 2 * s / ( t * t );
        m_fRaiseSpeed = 0f;
        m_nStatus = 1;

        m_fScaleSpeed = (m_fMaxScale - m_fInitScale) / m_fFirstSegTime;
        m_fFadeSpeed = -1f / m_fSecSegTime;

        SetScale( m_fInitScale );

        m_fAlpha = 1f;
    }

    void Loop()
    {
        vecTempPos = GetPos();
        vecTempPos.y += m_fRaiseSpeed * Time.fixedDeltaTime;
        vecTempPos.x = 0;
        vecTempPos.z = -100;
        m_fRaiseSpeed += m_fRaiseAccelerate * Time.fixedDeltaTime;
        SetPos( vecTempPos );

        if ( m_nStatus == 1 )
        {
            float fScale = GetScale();
            fScale += m_fScaleSpeed * Time.fixedDeltaTime;
            SetScale( fScale );

            if ( fScale >= m_fMaxScale )
            {
                m_nStatus = 2;
            }


        }
        else if (m_nStatus == 2 )
        {
            m_fAlpha += m_fFadeSpeed * Time.fixedDeltaTime;
            SetAlpha( m_fAlpha );

            if ( m_fAlpha <= 0 )
            {
                End();
            }
        }



    }

    public void End()
    {
        ResourceManager.s_Instance.DeleteRichTiaoZi( this.gameObject );
    }

    public void SetAlpha( float fAlpha )
    {
        colorTemp = _srIcon.color;
        colorTemp.a = fAlpha;
        _srIcon.color = colorTemp;

        colorTemp = _txtValue.color;
        colorTemp.a = fAlpha;
        _txtValue.color = colorTemp;
      

    }

    public void SetPos(Vector3 pos)
    {
        this.transform.localPosition = pos;
    }

    public Vector3 GetPos()
    {
        return this.transform.localPosition;
    }

    public float GetScale()
    {
        return this.transform.localScale.x;
    }

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetValue( int nValue )
    {
        _txtValue.text = nValue.ToString();
    }

}
