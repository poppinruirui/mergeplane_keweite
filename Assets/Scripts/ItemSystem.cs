﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSystem : MonoBehaviour {

    public static Vector3 vecTempScale = new Vector3();

    public static ItemSystem s_Instance = null;

    public GameObject _panelItemBag;

    public GameObject _containerUsing;
    public GameObject _containerNotUsing;

    List<UIItemInBag> m_lstUsingItems = new List<UIItemInBag>();

    int m_nItemRaise = 1;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    float m_fTimeElapse = 0f;
	void Update () {
        CountingLoop();
	}

    void CountingLoop()
    {
        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        for (int i = m_lstUsingItems.Count - 1; i >= 0; i-- ) // 如果中途可能删除节点，则应该反向遍历列表
        {
            UIItemInBag item = m_lstUsingItems[i];
            if ( item.DoCount() )
            {
                m_lstUsingItems.Remove( item );
                ResourceManager.s_Instance.DeleteUiItem( item );
                Main.s_Instance.UpdateRaise();
                continue;
            }

        } // end for 

    }

    public void OnClickButton_OpenBag()
    {
        _panelItemBag.SetActive(true);
    }


    public void OnClickButton_CloseBag()
    {
        _panelItemBag.SetActive( false );
    }

    public void AddItem( UIItemInBag item )
    {
        item.transform.SetParent(_containerNotUsing.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
    }

    public List<UIItemInBag> GetItemList()
    {
        return m_lstUsingItems;
    }

    public void UseItem(UIItemInBag item)
    {

        for (int i = 0; i < m_lstUsingItems.Count; i++ )
        {
            UIItemInBag item_using = m_lstUsingItems[i];
            if (item_using.m_nItemId == item.m_nItemId)
            {
                item_using.m_nDuration += item.m_nDuration;
                ResourceManager.s_Instance.DeleteUiItem(item);     
               
                return;
            }
        } // end foreach

//        item.SetCountingPanelVisible( true );
//        item.SetUseButtonVisible( false );
        item.m_StartTime = Main.GetSystemTime();
        m_lstUsingItems.Add(item);
        item.transform.SetParent(_containerUsing.transform);

        Main.s_Instance.UpdateRaise();


    }



} // END CLASS
