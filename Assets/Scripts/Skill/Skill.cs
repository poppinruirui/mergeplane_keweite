﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : MonoBehaviour {

    SkillManager.eSkillType m_eType = SkillManager.eSkillType.coin_raise;
    SkillManager.eSkillStatus m_eStatus = SkillManager.eSkillStatus.idle;


    public SkillManager.sSkillConfig m_Config;

    Planet m_BoundPlanet = null;
    District m_BoundDistrict = null;

    public UISkillCastCounter m_BoundUIBUtton = null;

    float m_fStartTime = 0;
    System.DateTime m_StartTime;

    public float m_fRealTimeDuration = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame

    
	void Update () {

        Loop_1_Second();
    }

    float m_fTimeElapse_1Second = 0f;
    void Loop_1_Second()
    {
        m_fTimeElapse_1Second += Time.deltaTime;
        if (m_fTimeElapse_1Second < 1f)
        {
            return;
        }
        m_fTimeElapse_1Second = 0f;

        WorkingLoop();
        ColdDownLoop();

    }

    public void SetStatus( SkillManager.eSkillStatus eStatus )
    {
        m_eStatus = eStatus;
    }

    public SkillManager.eSkillStatus GetStatus()
    {
        return m_eStatus;
    }

    public SkillManager.eSkillType GetSkillType()
    {
        return m_eType;
    }

    public void SetBound( Planet planet, District district )
    {
        m_BoundPlanet = planet;
        m_BoundDistrict = district;
    }

    public void SetType(SkillManager.eSkillType eType )
    {
        m_eType = eType;
    }

    public void SetConfig( SkillManager.sSkillConfig config  )
    {
        m_Config = config;
        UpdateUICounter();
    }

    public void UpdateUICounter()
    {
        /*
        if (!m_BoundPlanet.IsCurActivePlanet())
        {
            return;
        }

        SceneUiSkillButton button = SkillManager.s_Instance.GetSkillButton( m_eType );
      
        switch(m_eType)
        {
            case SkillManager.eSkillType.coin_raise:
                {
                //    button._txtCurValue.text = "X" + m_Config.fValue;

                }
                break;
            case SkillManager.eSkillType.cost_reduce:
                {
                  //  button._txtCurValue.text = "-" + ( m_Config.fValue * 100 ) + "%";

                }
                break;
            case SkillManager.eSkillType.speed_accelerate:
                {
               //     button._txtCurValue.text = "X" + m_Config.fValue;

                }
                break;

        } // end switch
       

        */
    }

    void WorkingLoop()
    {
        if ( GetStatus() != SkillManager.eSkillStatus.working )
        {
            return;
        }

        float fTimeElapse = (Main.GetSystemTime() - m_StartTime).Seconds;//Main.GetTime() - m_fStartTime;

     

        float fTimeLeft = m_fRealTimeDuration - (int)fTimeElapse;

        bool bCurDistrict = false;
        if ( this.m_BoundDistrict == MapManager.s_Instance.GetCurDistrict() )
        {
            bCurDistrict = true;
        }

        if (bCurDistrict)
        {
            m_BoundUIBUtton._txtLeftTime.text = fTimeLeft.ToString();
            m_BoundUIBUtton._imgProgress.fillAmount = (fTimeLeft / m_fRealTimeDuration );
        }

        if (fTimeLeft <= 0)
        {
            m_BoundUIBUtton._imgColdDownMask.gameObject.SetActive( true );
            m_BoundUIBUtton._imgColdDownMask.fillAmount = 1;

            SetStatus(SkillManager.eSkillStatus.colddown);
            SetStartTime(Main.GetSystemTime());
            switch ( m_eType )
            {
                case SkillManager.eSkillType.speed_accelerate:
                    {
                        if ( m_BoundDistrict == MapManager.s_Instance.GetCurDistrict() )
                        {
                            Main.s_Instance.StopAccelerateAll();
                        }
                    }
                    break;
                case SkillManager.eSkillType.coin_raise:
                    {
                        if (m_BoundDistrict == MapManager.s_Instance.GetCurDistrict())
                        {
                            Main.s_Instance.UpdateRaise();
                        }
                    }
                    break;
                case SkillManager.eSkillType.cost_reduce:
                    {
                        if (m_BoundDistrict == MapManager.s_Instance.GetCurDistrict())
                        {
                            TanGeChe.s_Instance.UpdateCarMallInfo();
                        }
                    }
                    break;

            } // end switch

            if (bCurDistrict)
            {
             
            //    m_BoundUIBUtton._txtStatus.text = "ColdDown...";
            }
        }
    }

    void ColdDownLoop()
    {
        if (GetStatus() != SkillManager.eSkillStatus.colddown)
        {
            return;
        }


        float fTimeElapse = ( Main.GetSystemTime() - m_StartTime ).Seconds ;// Main.GetTime() - m_fStartTime;
        float fTimeLeft = m_Config.nColdDown - (int)fTimeElapse;

        m_BoundUIBUtton._imgColdDownMask.fillAmount = fTimeLeft / m_Config.nColdDown;

        bool bCurDistrict = false;
        if (this.m_BoundDistrict == MapManager.s_Instance.GetCurDistrict())
        {
            bCurDistrict = true;
        }

        if (bCurDistrict)
        {
            m_BoundUIBUtton._txtLeftTime.text = fTimeLeft.ToString();
        }

        if (fTimeLeft <= 0)
        {
            m_BoundUIBUtton._imgColdDownMask.gameObject.SetActive(false);
            SetStatus(SkillManager.eSkillStatus.idle);
            if (bCurDistrict)
            {
                m_BoundUIBUtton._txtLeftTime.text = "";
                //  m_BoundUIBUtton._txtStatus.text = "(可用)";
                m_BoundUIBUtton._imgProgress.fillAmount = 1;
                m_BoundUIBUtton._imgProgress.gameObject.SetActive(false);
            }
        }
    }

    public void SetStartTime( System.DateTime StartTime/*float fStartTime*/ )
    {
        // m_fStartTime = fStartTime;
        m_StartTime = StartTime;
    }


    public System.DateTime GetStartTime()
    {
        return m_StartTime;
    }


} // end class
