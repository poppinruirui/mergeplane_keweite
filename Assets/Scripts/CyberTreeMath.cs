﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberTreeMath : MonoBehaviour {

    public const float ANGLE_TO_RADIAN_XISHU = 0.017453f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static float Angle2Radian(float fAngle)
    {
        return ANGLE_TO_RADIAN_XISHU * fAngle;
    }

    public static float Cos(float fAngle)
    {
        return Mathf.Cos( Angle2Radian( fAngle ) );
    }

    public static float Sin(float fAngle)
    {
        return Mathf.Sin(Angle2Radian(fAngle));
    }

    // 已知位移和时间求初速度（末速度为零）
    public static float GetV0(float s, float t)
    {
        return (2.0f * s / t);
    }

    // 已知位移和时间求加速度（末速度为零）
    public static float GetA(float s, float t)
    {
        return -2.0f * s / (t * t);
    }



}
