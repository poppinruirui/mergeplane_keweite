﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour {


    public District[] m_aryDistricts;

    public GameObject _uiPlanetLock;
    public SceneUiPrice _uiUnlockPlanetPrice;

    public MapManager.ePlanetStatus m_eStatus = MapManager.ePlanetStatus.unlocked;
    public int m_nId = 0;

    public int m_nUnlockPrice = 0; //  解锁该星球所需的金币数量

    // poppin test
    int m_nCoin = 100000; // 金币是属于某个星球的，绿票是属于整个玩家的

   

	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetStatus(MapManager.ePlanetStatus eStatus)
    {
        m_eStatus = eStatus;
    }

    public MapManager.ePlanetStatus GetStatus( )
    {
        return m_eStatus;
    }

    public int GetId()
    {
        return m_nId;
    }

    public District GetDistrictById( int nId )
    {
        if ( nId < 0 || nId >= m_aryDistricts.Length )
        {
            return null;
        }
        return m_aryDistricts[nId];
    }

    public void AddCoin( int nValue )
    {
        m_nCoin += nValue;
        Main.s_Instance._moneyCoin.SetValue(m_nCoin);
    }

    public void CostCoin( int nValue )
    {
        m_nCoin -= nValue;
        Main.s_Instance._moneyCoin.SetValue(m_nCoin);
    }

    public int GetCoin()
    {
        return m_nCoin;
    }

    public void SetCoin( int nValue )
    {
        m_nCoin = nValue;

        if (this == MapManager.s_Instance.GetCurPlanet())
        {
            Main.s_Instance._moneyCoin.SetValue(m_nCoin);

            for (int i = 0; i < Main.s_Instance.m_aryActivePlanetCoin.Length; i++ )
            {
                MoneyCounter counter = Main.s_Instance.m_aryActivePlanetCoin[i];
                if ( counter == null )
                {
                    continue;
                }
                counter.SetValue( nValue );
            }
        }

        MoneyCounter[] aryCounters = null;
        switch( GetId() )
        {
            case 0:
                {
                    aryCounters = Main.s_Instance.m_aryCoin0;
                }
                break;
            case 1:
                {
                    aryCounters = Main.s_Instance.m_aryCoin1;
                }
                break;
            case 2:
                {
                    aryCounters = Main.s_Instance.m_aryCoin2;
                }
                break;
        } // end switch

        for (int i = 0; i < aryCounters.Length; i++ )
        {
            MoneyCounter counter = aryCounters[i];
            if ( counter == null )
            {
                continue;
            }
            counter.SetValue( nValue );
        }


    }

    public bool CheckIfAllDistrictsUnlocked()
    {
        for (int i = 0; i < m_aryDistricts.Length; i++ )
        {
            District district = m_aryDistricts[i];
            if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
            {
                return false;
            }
        }

        return true;
    }

    public int GetUnlockCoinCost()
    {
        return m_nUnlockPrice;
    }

    public void DoUnlock()
    {
        SetStatus(MapManager.ePlanetStatus.unlocked);
        District district_0 = GetDistrictById(0);
        district_0.SetStatus(MapManager.eDistrictStatus.unlocked);
    }

    public District[] GetDistrictsList()
    {
        return m_aryDistricts;
    }

    public bool IsCurActivePlanet()
    {
        return this == MapManager.s_Instance.GetCurPlanet();
    }

} // end class
