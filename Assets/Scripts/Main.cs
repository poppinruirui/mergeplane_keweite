﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public static Vector3 vecTempPos = new Vector3();
    public static Vector3 vecTempScale = new Vector3();

    public static Main s_Instance = null;

    public float m_nCurRaise = 1;

    public MoneyCounter[] m_aryCoin0;
    public MoneyCounter[] m_aryCoin1;
    public MoneyCounter[] m_aryCoin2;

    //// ----------
    /// UI
    /// -----------
    public GameObject _panelCollectOfflineProfit;
    public Text _txtOfflineProfitOfThisDistrict;

    public Button _btnOpenBigMap;
    public Button _btnCloseBigMap;
    public MoneyCounter _moneyCoin;
    public MoneyCounter[] m_aryActivePlanetCoin;
    public MoneyCounter _moneyGreenCash;   

    public GameObject _panelRaiseDetail; // “提升概览”面板
    public Text _txtRaiseDetail; // 显示“提升概览”的内容

    public SceneUiButton _btnBuy;

    public GameObject _BigMap;
    public GameObject _panelPrestige;
    public UIPrestige _Prestige;

    public TextMesh _txtNumOfRunningPlane;
    public Text _txtDPS;

    /// <summary>
    /// / SceneUI
    /// </summary>
    public GameObject _goRecycleBox;

    public float DISTANCE_UNIT = 3f;
    public float TURN_RADIUS = 0f;
    public Vector2 START_POS = Vector2.zero;
    public float LENGTH_OF_TURN = 0f; // 转角处的长度　
    public float PERIMETER = 0f;

    public float MERGE_OFFSET = 0.5f;
    public float MERGE_TIME = 0.2f;
    public float MERGE_WAIT_TIME = 0.2f;

    public GameObject _containerRunningPlanes;

    List<Plane> m_lstRunningPlanes = new List<Plane>();

    public float[] m_aryPosThreshold = new float[8];
    public Vector2[] m_aryStartPosOfEachSegment = new Vector2[8];
    public Vector2[] m_aryCircleCenterOfTurn = new Vector2[8];

    public float m_fMergeMoveSpeed = 0f;

    public Lot[] m_aryLots;

    public StartBelt m_StartBelt;

    public float[] m_aryRoundTimeByLevel;

    public enum ePosType
    {
        left,
        left_top,
        top,
        right_top,
        right,
        right_bottom,
        bottom,
        left_bottom
    };

    public enum ePlaneStatus
    {
        idle,
        running_on_airline,
        dragging,
        running_to_lot,
        merging,
        treasure_box,
    };

    public static float GetTime()
    {
        return Time.time;
    }

    public static System.DateTime GetSystemTime()
    {
        return System.DateTime.Now;
    }
    const float PARAM1 = 0.4f;
    private void Awake()
    {
        s_Instance = this;

        TURN_RADIUS = DISTANCE_UNIT * 1.5f;
        START_POS = new Vector2(-DISTANCE_UNIT, 0f);
        float fTotalLengthOfTurns = 2f * Mathf.PI * TURN_RADIUS;
        PERIMETER = 8f * DISTANCE_UNIT + fTotalLengthOfTurns;
        LENGTH_OF_TURN = fTotalLengthOfTurns / 4f;

        // old
        /*
        m_aryPosThreshold[(int)ePosType.left] = 1.5f * DISTANCE_UNIT;
        m_aryPosThreshold[(int)ePosType.top] = 0.5f * DISTANCE_UNIT;
        m_aryPosThreshold[(int)ePosType.right] = -1.5f * DISTANCE_UNIT;
        m_aryPosThreshold[(int)ePosType.bottom] = -0.5f * DISTANCE_UNIT;

        m_aryStartPosOfEachSegment[(int)ePosType.left] = new Vector2( -1.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT );
        m_aryStartPosOfEachSegment[(int)ePosType.left_top] = new Vector2(-1.5f * DISTANCE_UNIT, 1.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.top] = new Vector2(-0.5f * DISTANCE_UNIT, 2.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right_top] = new Vector2(0.5f * DISTANCE_UNIT, 2.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right] = new Vector2( 1.5f * DISTANCE_UNIT, 1.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right_bottom] = new Vector2(1.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.bottom] = new Vector2( 0.5f * DISTANCE_UNIT, -2.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.left_bottom] = new Vector2(-0.5f * DISTANCE_UNIT, -2.5f * DISTANCE_UNIT);

        m_aryCircleCenterOfTurn[(int)ePosType.left_top] = new Vector2( -0.5f * DISTANCE_UNIT,  1.5f * DISTANCE_UNIT );
        m_aryCircleCenterOfTurn[(int)ePosType.right_top] = new Vector2(0.5f * DISTANCE_UNIT, 1.5f * DISTANCE_UNIT);
        m_aryCircleCenterOfTurn[(int)ePosType.right_bottom] = new Vector2(0.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT);
        m_aryCircleCenterOfTurn[(int)ePosType.left_bottom] = new Vector2(-0.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT);
        // end old
        */
        // new 


        m_aryCircleCenterOfTurn[(int)ePosType.left_top] = new Vector2(-0.1f * DISTANCE_UNIT, 1.2f * DISTANCE_UNIT);
        m_aryCircleCenterOfTurn[(int)ePosType.right_top] = new Vector2(0.1f * DISTANCE_UNIT, 1.2f * DISTANCE_UNIT);
        m_aryCircleCenterOfTurn[(int)ePosType.right_bottom] = new Vector2(0.1f * DISTANCE_UNIT, -1.1f * DISTANCE_UNIT);
        m_aryCircleCenterOfTurn[(int)ePosType.left_bottom] = new Vector2(-0.1f * DISTANCE_UNIT, -1.1f * DISTANCE_UNIT);

        m_aryPosThreshold[(int)ePosType.left] = m_aryCircleCenterOfTurn[(int)ePosType.left_top].y;
      

       

        m_aryStartPosOfEachSegment[(int)ePosType.left] = new Vector2(-1.6f * DISTANCE_UNIT, -1.2f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.left_top] = new Vector2(-0.8f * DISTANCE_UNIT, 1.2f * DISTANCE_UNIT);


        m_aryStartPosOfEachSegment[(int)ePosType.top] = new Vector2(m_aryCircleCenterOfTurn[(int)ePosType.left_top].x, m_aryCircleCenterOfTurn[(int)ePosType.left_top].y + TURN_RADIUS);

        m_aryPosThreshold[(int)ePosType.top] = m_aryCircleCenterOfTurn[(int)ePosType.right_top].x;

        m_aryPosThreshold[(int)ePosType.right] = m_aryCircleCenterOfTurn[(int)ePosType.right_bottom].y;

        m_aryStartPosOfEachSegment[(int)ePosType.bottom] = new Vector2(m_aryCircleCenterOfTurn[(int)ePosType.right_bottom].x, m_aryCircleCenterOfTurn[(int)ePosType.right_bottom].y - TURN_RADIUS);


        m_aryStartPosOfEachSegment[(int)ePosType.right_top] = new Vector2(0.8f * DISTANCE_UNIT, 1.2f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right] = new Vector2(1.5f * DISTANCE_UNIT, 1.2f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right_bottom] = new Vector2(1.5f * DISTANCE_UNIT, -1.2f * DISTANCE_UNIT);

        m_aryStartPosOfEachSegment[(int)ePosType.left_bottom] = new Vector2(-1.5f * DISTANCE_UNIT, -1.2f * DISTANCE_UNIT);


        m_aryPosThreshold[(int)ePosType.bottom] = m_aryCircleCenterOfTurn[(int)ePosType.left_bottom].x;



        // end new



        m_fMergeMoveSpeed = MERGE_OFFSET / MERGE_TIME * Time.fixedDeltaTime;


        m_aryRoundTimeByLevel = new float[32];
        for (int i = 0; i < m_aryRoundTimeByLevel.Length; i++ )
        {
            m_aryRoundTimeByLevel[i] = 6f - 0.2f * i;
        }

        /// 注册事件
//        _btnBuy.OnClickEvent += new SceneUiButton.OnClickEventHandler(OnClickButton_BuyPlane); // 订阅举杯事件



    }

    // Use this for initialization
    void Start()
    {
      

        // 读档............
        // to do

        MapManager.s_Instance.Init();

        DebugInfo.s_Instance.SetPlanetAndDistrict(0, 0);
    }

    // Update is called once per frame

    float m_f10SecLoopElapse = 0f;
    void Update()
    {
        Ten_Sec_Loop();

        PreAccelerateLoop();
    }

    void Ten_Sec_Loop()
    {

        m_f10SecLoopElapse += Time.deltaTime;
        if (m_f10SecLoopElapse < 10f)
        {
            return;
        }
        m_f10SecLoopElapse = 0;

        float fDps = MapManager.s_Instance.GetCurDistrict().CalculateDPS();
        _txtDPS.text = (int)fDps + "/秒"; 
    }

    private void FixedUpdate()
    {
      
    }

   

    // 根据飞机等级，获取该飞机运行轨道一周所需的时间
    public float GetOneRoundTimeByLevel( int nLevel )
    {
        return DataManager.s_Instance.GetVehicleRunTimePerRound( nLevel );
    }

    public Lot GetLotById( int nId )
    {
        if ( nId < 0 || nId >= m_aryLots.Length )
        {
            return null;
        }
        return m_aryLots[nId];
    }

    public void OnClickButton_BuyPlane()
    {
        if ( UIManager.IsPointerOverUI() )
        {
            return;
        }

        Lot lot = null;

        // 先判断有没有空位
        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            if ( !m_aryLots[i].IsTaken() )
            {
                lot = m_aryLots[i];
                break;
            }
        }

        if ( lot == null )
        {
            UIMsgBox.s_Instance.ShowMsg("停机坪没有空位了"); 
            return;
        }

        Plane plane = ResourceManager.s_Instance.NewPlane();
        plane.SetLevel(1);
        lot.SetPlane( plane );

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);

    }

    public bool BuyOneVehicle( int nLevel )
    {
        Lot lot = null;

        // 先判断有没有空位
        for (int i = 0; i < m_aryLots.Length; i++)
        {
            if (!m_aryLots[i].IsTaken())
            {
                lot = m_aryLots[i];
                break;
            }
        }

        if (lot == null)
        {
            UIMsgBox.s_Instance.ShowMsg("停机坪没有空位了");
            return false;
        }


        Plane plane = ResourceManager.s_Instance.NewPlane();
        plane.SetLevel(nLevel);
        lot.SetPlane(plane);

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);

        return true;
    }

    public void BeginRun( Plane plane )
    {
        plane.transform.SetParent(_containerRunningPlanes.transform);
        plane.SetPos( START_POS );
        m_lstRunningPlanes.Add( plane );
        m_StartBelt.SetRunningPlanesNum( m_lstRunningPlanes.Count );
                          
    }

    public void RemovePlaneFromeAirline( Plane plane )
    {
        m_lstRunningPlanes.Remove( plane );

        m_StartBelt.SetRunningPlanesNum(m_lstRunningPlanes.Count);
    }

    public bool CheckIfCanRun()
    {
        return m_StartBelt.CheckIfCanRun();
    }

    public void ShowCanMergeLots( bool bVisible, Plane planeDragging = null )
    {
        if ( !bVisible )
        {
            for (int i = 0; i < m_aryLots.Length; i++)
            {
                Lot lot = m_aryLots[i];
                lot.SetEffectCanMergeVisible(false);

            }
        }
        else
        {
            for (int i = 0; i < m_aryLots.Length; i++ )
            {
                Lot lot = m_aryLots[i];
                if ( lot.IsTaken() )
                {
                    Plane plane = lot.GetPlane();
                    if ( plane != planeDragging && plane.GetLevel() == planeDragging.GetLevel() && plane.GetPlaneStatus() == ePlaneStatus.idle )
                    {
                        lot.SetEffectCanMergeVisible( true );
                    }
                }
            }
        }
    }

    public Lot GetOneAvailableLotToGenerateBox()
    {
        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            if ( !m_aryLots[i].IsTaken() )
            {
                return m_aryLots[i];
            }
        }

        return null;
    }

    public string GenerateDistrictData()
    {
        string szData = "";

        return szData;
    }

    public void OnClickButton_OpenBigMap()
    {
        _BigMap.SetActive( true );
        _btnOpenBigMap.gameObject.SetActive( false );

        UIManager.s_Instance.SetSomeUiVisibleDueToStarSky( false );
        MapManager.s_Instance.StartStarSkyEffect();

        MapManager.s_Instance.UpdateUIPlanetsInfo();
    }

    public void OnClickButton_CloseBigMap()
    {
        _BigMap.SetActive(false);
        _btnOpenBigMap.gameObject.SetActive(true);

        UIManager.s_Instance.SetSomeUiVisibleDueToStarSky(true);

        MapManager.s_Instance.StopStarSkyEffect();
    }

    public void OnClickButton_OpenPrestigePanel()
    {
        _panelPrestige.SetActive( true );
        _Prestige.UpdateInfo();
    }

    public void OnClickButton_ClosesPrestigePanel()
    {
        _panelPrestige.SetActive(false);
    }

    public string GenerateData()
    {
        string szData = "";
        
        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            Lot lot = m_aryLots[i];
            Plane plane = lot.GetPlane();
            if ( plane == null )
            {
                continue;
            }
            szData +=((int)(plane.GetPlaneStatus()) + "," );
            szData += (lot.GetId() + ","); // 停机坪的编号
            szData += ((plane.GetLevel()) + ","); // 该飞机的等级 
            szData += ( (int)plane.GetPosType() + "," + plane.GetPos().x.ToString("f2") + "," + plane.GetPos().y.ToString("f2")); //  该飞机的当前位置
            szData += "|";

        }


   //     Debug.Log( "szData = " + szData);


        return szData;
    }

    public void ClearAll()
    {
        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            Lot lot = m_aryLots[i];
            Plane plane = lot.GetPlane();
            if (plane != null )
            {
                ResourceManager.s_Instance.DeletePlane( plane );
                lot.SetPlane(null);
            }

        }

        // 跑道的占用清空
        StartBelt.s_Instance.SetRunningPlanesNum(0);
        m_lstRunningPlanes.Clear();

    }

    public void Load( Planet planet, District district )
    {
        ClearAll();

        string szData = district.GetData();

        if (szData == "")
        {
            return;
        }

        string[] aryData = szData.Split( '|' );
        for (int i = 0; i < aryData.Length; i++ )
        {
            if (aryData[i] == "")
            {
                continue;
            }
            string[] aryParams = aryData[i].Split( ',' );
           
                int nStatus = int.Parse(aryParams[0]);
                int nLotId = int.Parse(aryParams[1]);
                int nPlaneLevel = int.Parse(aryParams[2]);
                ePosType posType = (ePosType)(int.Parse(aryParams[3]));
                float fPosX = float.Parse(aryParams[4]);
                float fPosY = float.Parse(aryParams[5]);

                Lot lot = GetLotById(nLotId);

                Plane plane = ResourceManager.s_Instance.NewPlane();
                plane.SetLevel(nPlaneLevel);
                lot.SetPlane(plane);


                if (nStatus == (int)ePlaneStatus.running_on_airline) // 正在跑道上跑
                {
                    lot.SetBoundPlaneAvatarVisible(true);
                    plane.BeginRun();

                    plane.SetPosType(posType);
                    vecTempPos.x = fPosX;
                    vecTempPos.y = fPosY;
                     plane.SetPos( vecTempPos );
                }
                else
                {

                }

          
        } // end for i



    }

    // 打开“提升概览”面板
    public void OnClick_OpenRaiseDetail()
    {
        _panelRaiseDetail.SetActive( true );

        UpdateRaise();
    }

    public void UpdateRaise()
    {
        UpdateRaise(MapManager.s_Instance.GetCurPlanet(), MapManager.s_Instance.GetCurDistrict());
    }

    // poppin to do 统一一下规则，提升比例到底是直接乘还是“1+”
    public float UpdateRaise( Planet cur_planet, District cur_district, bool bOffline = false)
    {
        float nCurRaise = 1;
        //m_nCurRaise = 1;
        _txtRaiseDetail.text = "";
        string szInfo = "";

        // 统计当前的提升明细
      //  Planet cur_planet = MapManager.s_Instance.GetCurPlanet();
      //  District cur_district = MapManager.s_Instance.GetCurDistrict();
        int nPlanetId = cur_planet.GetId();
        int nDistrictId = cur_district.GetId();
        int nPrestigeTimes = cur_district.GetPrestigeTimes();

        bool bFirst = true;


        // 道具强化
        int nItemRaise = 0;
        List<UIItemInBag> lstItems = ItemSystem.s_Instance.GetItemList();
        for (int i = 0; i < lstItems.Count; i++)
        {
            if (bFirst)
            {
                szInfo += "  ";
            }

            bFirst = false;
            UIItemInBag item = lstItems[i];
            szInfo += item.m_nValue0 + "倍  强化道具" + "\n";
            nItemRaise += item.m_nValue0;

            if ( i != lstItems.Count - 1)
            {
                szInfo += "+";
            }
            else
            {
             
            }

        }

        if (lstItems.Count >1)
        {
            szInfo += "=" + nItemRaise + "倍\n";
        }
        szInfo += "--------------------------------\n";

        if (nItemRaise > 0)
        {
            nCurRaise/*m_nCurRaise*/ *= nItemRaise;
        }

        if (bFirst)
        {
            szInfo += "  ";
        }



        // 重生加成
        int nPresitageRaise = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nPrestigeTimes);
        if (nPrestigeTimes > 0)
        {
            szInfo += "X" + nPresitageRaise + "倍  重生加成" + "\n";
            nCurRaise/*m_nCurRaise*/ *= nPresitageRaise;

            bFirst = false;
        }




        // (主动)技能加成
        if (!bOffline) // 离线状态主动技能无效
        {
            Skill skill = cur_district/*MapManager.s_Instance.GetCurDistrict()*/.GetSkill(SkillManager.eSkillType.coin_raise);
            if (skill.GetStatus() == SkillManager.eSkillStatus.working)
            {
                float fSkillRaise = skill.m_Config.fValue;
                float fSKillRaiseRaise = ScienceTree.s_Instance.GetSkillCoinRaiseRaise();
                if (fSKillRaiseRaise > 0)
                {
                    fSkillRaise *= (1 + fSKillRaiseRaise);
                    szInfo += "X" + fSkillRaise + "倍  主动技能加成(含" + fSKillRaiseRaise + "技能树加成)\n";
                }
                else
                {
                    szInfo += "X" + fSkillRaise + "倍  主动技能加成" + "\n";
                }
                nCurRaise/*m_nCurRaise*/ *= fSkillRaise;

                bFirst = false;
            }
        } // end if (!bOffline)

        // 科技树加成
        float fScienceTreeRaise = ScienceTree.s_Instance.GetCoinRaise(nPlanetId, nDistrictId);
        if (fScienceTreeRaise > 0)
        {
                szInfo += "X" + fScienceTreeRaise + "倍  科技树加成" + "\n";
                nCurRaise/*m_nCurRaise*/ *= fScienceTreeRaise;

                bFirst = false;
        }
      


        // 广告收益
        float fAdsRaise = cur_district/*MapManager.s_Instance.GetCurDistrict()*/.GetAdsRaise();    
        if (fAdsRaise > 0)
        {
            szInfo += "X" + fAdsRaise + "倍  广告收益" + "\n";
            nCurRaise/*m_nCurRaise*/ *= fAdsRaise;
        }



        szInfo += "=====================\n";
        if (nCurRaise/*m_nCurRaise*/ > 1)
        {
            szInfo += "= " + m_nCurRaise + "倍";
        }

        if (cur_district == MapManager.s_Instance.GetCurDistrict())
        {
            _txtRaiseDetail.text = szInfo;
            m_nCurRaise = nCurRaise;
        }

        return nCurRaise;
    }

    public float GetRaise()
    {
        return m_nCurRaise;
    }

    public void OnClick_CloseRaiseDetail()
    {
        _panelRaiseDetail.SetActive(false);
    }

    float m_fAccelerate = 0f;
    float m_fPreAccerlateTimeLeft = 0;
    public void PreAccelerateAll(float fAccelerate)
    {
        m_fAccelerate = fAccelerate;

        m_fPreAccerlateTimeLeft = 1f;
    }

    void PreAccelerateLoop()
    {
        if (m_fPreAccerlateTimeLeft <= 0)
        {
            return;
        }

        m_fPreAccerlateTimeLeft -= Time.deltaTime;
        if (m_fPreAccerlateTimeLeft <= 0)
        {
            SkillManager.s_Instance.m_arySkillButton[0]._effectLight.EndPlay();
            SkillManager.s_Instance.m_arySkillButton[0]._effectLight.gameObject.SetActive( false );

            AccelerateAll(m_fAccelerate);

            for (int i = 0; i < m_lstRunningPlanes.Count; i++ )
            {
               
                Plane plane = m_lstRunningPlanes[i];
                EffectManager.s_Instance.DeleteEffect(plane.GetAccelerateLight());
                plane.SetAccelerateLight(null);

               
            }
        }

    }

    public void AccelerateAll( float fAccelerate )
    {
        for (int i = 0; i < m_lstRunningPlanes.Count; i++ )
        {
            Plane plane = m_lstRunningPlanes[i];
            plane.BeginAccelerate( 100000f, fAccelerate);
        }
    }

    public void StopAccelerateAll()
    {
        for (int i = 0; i < m_lstRunningPlanes.Count; i++)
        {
            Plane plane = m_lstRunningPlanes[i];
            plane.EndAccelerate();
        }
    }

     
    public List<Plane> GetRunningList()
    {
        return m_lstRunningPlanes;
    }

    bool m_bLostFocus = false;
    private object _panelAds;
    public float m_fOfflineProfit = 0;
    void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus)// has focus
        {
            if (m_bLostFocus)
            {
                District district = MapManager.s_Instance.GetCurDistrict();
                district.OnLine();
                m_fOfflineProfit = district.CalculateOffLineProfit();
                DebugInfo.s_Instance._txtPlanetAndDistrictId.text += " | " + Main.GetTime();
                if (m_fOfflineProfit > 0)
                {
                    _panelCollectOfflineProfit.SetActive(true);
                    _txtOfflineProfitOfThisDistrict.text = m_fOfflineProfit.ToString("f0");
                }
            }
            m_bLostFocus = false;
        }
        else // lost focus
        {
            MapManager.s_Instance.GetCurDistrict().OffLine();
            m_bLostFocus = true;
            //DebugInfo.s_Instance._txtPlanetAndDistrictId.text = "到底有米有" + Main.GetTime();
        }
    }

    public void OnClick_CollectOfflineProfit()
    {
        Planet planet = MapManager.s_Instance.GetCurPlanet();
        planet.SetCoin(planet.GetCoin() + (int)m_fOfflineProfit);

        MapManager.s_Instance.GetCurDistrict().SetCurTotalOfflineGain(0);
        _panelCollectOfflineProfit.SetActive(false);

        vecTempScale.x = 2f;
        vecTempScale.y = 2f;
        vecTempScale.z = 2f;

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_bank);

        for (int i = 0; i < 30; i++)
        {
            UIFlyingCoin coin = ResourceManager.s_Instance.NewFlyingCoin();
            coin.transform.SetParent(UIManager.s_Instance._containerFlyingCoins.transform);

            coin.transform.localScale = vecTempScale;
            coin.transform.localPosition = UIManager.s_Instance.m_vecCoinFlyStartPos;

            coin.m_vecSession0End.x = UnityEngine.Random.Range(UIManager.s_Instance.m_vecCoinFlySeesion0EndPosRangeX.x, UIManager.s_Instance.m_vecCoinFlySeesion0EndPosRangeX.y);
            coin.m_vecSession0End.y = UnityEngine.Random.Range(UIManager.s_Instance.m_vecCoinFlySeesion0EndPosRangeY.x, UIManager.s_Instance.m_vecCoinFlySeesion0EndPosRangeY.y);

            coin.m_vecEndPos = UIManager.s_Instance.m_vecCoinFlyEndPos;

            float sX = coin.m_vecSession0End.x - coin.transform.localPosition.x;
            float sY = coin.m_vecSession0End.y - coin.transform.localPosition.y;
            float t = UIManager.s_Instance.m_fSession0Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession0_A.x = 2f * sX / (t * t);
            coin.m_vecSession0_A.y = 2f * sY / (t * t);

            sX = UIManager.s_Instance.m_vecCoinFlyEndPos.x - coin.m_vecSession0End.x;
            sY = UIManager.s_Instance.m_vecCoinFlyEndPos.y - coin.m_vecSession0End.y;
            t = UIManager.s_Instance.m_fSession1Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession1_A.x = 2f * sX / (t * t);
            coin.m_vecSession1_A.y = 2f * sY / (t * t);


            coin.BeginFly();
        } // end for
    }

    public void OnClick_CollectOfflineProfit_WithAds()
    {
        AdsManager.s_Instance.SetAdsType(AdsManager.eAdsType.collect_offline_profit_x2);
        AdsManager.s_Instance._panelAds.SetActive(true);
        _panelCollectOfflineProfit.SetActive(false);
    }


} // end class

