﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager s_Instance = null;

    public Sprite m_sprArrow;
    public Sprite[] m_arySkillPointIcon;
    public Sprite[] m_aryCoinIcon;

    public GameObject m_goRecycledPlanes;

    public Sprite[] m_aryPlaneSprites;
    public Sprite[] m_aryParkingPlaneSprites;

    public GameObject m_prePlane; // prefab of plane


    public GameObject m_preJinBi;
    public GameObject m_preRichTiaoZi;

    public GameObject m_preVehicleCounter;

    public Color[] m_aryTrailColor;

    public GameObject m_preSkill;

    public GameObject m_preFlyingCoin;

    /// <summary>
    /// 科技树 
    /// </summary>
    public GameObject m_preScienceTreeConfig;
    public GameObject m_preScienceLeaf;
    //// end 科技树

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Sprite GetPlaneSpriteByLevel( int nLevel )
    {
        return m_aryPlaneSprites[nLevel - 1];
    }

    public Sprite GetParkingPlaneSpriteByLevel(int nLevel)
    {
        return m_aryParkingPlaneSprites[nLevel - 1];
    }

    static int s_TrailColorIndex = 0;

    List<Plane> m_lstRecycledPlanes = new List<Plane>();
    public Plane NewPlane()
    {
        Plane plane = null;

        if ( m_lstRecycledPlanes.Count > 0 )
        {
            plane = m_lstRecycledPlanes[0];
            plane.gameObject.SetActive( true );
            plane.Reset();
            m_lstRecycledPlanes.RemoveAt(0);
        }
        else
        {
            plane = GameObject.Instantiate(m_prePlane).GetComponent<Plane>();
        }

        // poppin test
        if (s_TrailColorIndex >= m_aryTrailColor.Length)
        {
            s_TrailColorIndex = 0;
        }
        plane.SetTrailColor(m_aryTrailColor[s_TrailColorIndex++]);

        return plane;
    }

    public void DeletePlane( Plane plane )
    {
        plane.gameObject.SetActive( false );
        m_lstRecycledPlanes.Add( plane );
        plane.transform.SetParent( m_goRecycledPlanes.transform );
    }

    List<GameObject> m_lstRecycledJinBi = new List<GameObject>();
    public GameObject NewJinBi()
    {
        GameObject goJinBi = null;
        if ( m_lstRecycledJinBi.Count > 0 )
        {
            goJinBi = m_lstRecycledJinBi[0];
            m_lstRecycledJinBi.RemoveAt(0);
            goJinBi.SetActive(true);
        }
        else
        {
            goJinBi = GameObject.Instantiate(m_preJinBi);
        }
        return goJinBi;
    }

    public void DeleteJinBi( GameObject goJinBi )
    {
        goJinBi.SetActive( false );
        m_lstRecycledJinBi.Add( goJinBi );
    }

    List<GameObject> m_lstRecycledRichTiaoZi = new List<GameObject>();
    public GameObject NewRichTiaoZi()
    {
        GameObject tiaozi = null;
        if ( m_lstRecycledRichTiaoZi.Count > 0 )
        {
            tiaozi = m_lstRecycledRichTiaoZi[0];
            m_lstRecycledRichTiaoZi.RemoveAt(0);
            tiaozi.SetActive( true );
        }
        else
        {
            tiaozi = GameObject.Instantiate(m_preRichTiaoZi);
        }
        return tiaozi;
    }

    public void DeleteRichTiaoZi(GameObject tiaozi)
    {
        tiaozi.SetActive( false );
        m_lstRecycledRichTiaoZi.Add( tiaozi);
    }

    ////// 商城道具相关
    public GameObject m_preUiItem;
    List<UIItemInBag> m_lstRecylcedUiItems = new List<UIItemInBag>();
    public GameObject _containerRecycledUIItems;
    public UIItemInBag NewUiItem()
    {
        UIItemInBag item = null;
        if (m_lstRecylcedUiItems.Count > 0)
        {
            item = m_lstRecylcedUiItems[0];
            item.gameObject.SetActive( true );
            m_lstRecylcedUiItems.RemoveAt(0);
        }
        else
        {
            item = GameObject.Instantiate(m_preUiItem).GetComponent<UIItemInBag>();
        }
        return item;
    }

    public void DeleteUiItem(UIItemInBag item)
    {
        item.gameObject.SetActive(false);
        m_lstRecylcedUiItems.Add( item );
        item.transform.SetParent(_containerRecycledUIItems.transform);
    }

    public const int MAX_VEHICLE_NUM = 13;

    public UIVehicleCounter NewVehicleCounter()
    {
        return GameObject.Instantiate( m_preVehicleCounter ).GetComponent<UIVehicleCounter>();
    }


    ////// end 商城道具相关

    public Skill NewSkill()
    {
        return GameObject.Instantiate(m_preSkill).GetComponent<Skill>();
    }

    //// 科技树
    public ScienceTreeConfig NewTreeConfig()
    {
        return GameObject.Instantiate(m_preScienceTreeConfig).GetComponent<ScienceTreeConfig>();
    }

    public ScienceLeaf newScienceLeaf()
    {
        return GameObject.Instantiate(m_preScienceLeaf).GetComponent<ScienceLeaf>();
    }

    /// end 科技树
    List<UIFlyingCoin> m_lstRecycledFlyingCoin = new List<UIFlyingCoin>();
    public UIFlyingCoin NewFlyingCoin()
    {
        UIFlyingCoin coin = null;

        if (m_lstRecycledFlyingCoin.Count > 0)
        {
            coin = m_lstRecycledFlyingCoin[0];
            coin.gameObject.SetActive( true );
            m_lstRecycledFlyingCoin.RemoveAt(0);
        }
        else
        {
            coin = GameObject.Instantiate( m_preFlyingCoin ).GetComponent<UIFlyingCoin>();
        }

        return coin;
    }


    public void DeleteFlyingCoin(UIFlyingCoin coin )
    {
        coin.gameObject.SetActive( false )  ;
        m_lstRecycledFlyingCoin.Add( coin );
    }


    public Sprite GetCoinSpriteByPlanetId( int nPlanetId )
    {
        return m_aryCoinIcon[nPlanetId];
    }


} // end class
