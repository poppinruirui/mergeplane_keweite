﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TanGeChe : MonoBehaviour {

    public static Vector3 vecTempScale = new Vector3();

    public static TanGeChe s_Instance = null;

    public GameObject _goCounterList;
    List<UIVehicleCounter> m_lstVehicleCounters = new List<UIVehicleCounter>();

    public GameObject _panelTanGeChe;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadData( int nPlanetId, int nDistrictId )
    {
        for (int i = 0; i < ResourceManager.MAX_VEHICLE_NUM; i++ )
        {

            int nLevel = i + 1;
            float nInitialPrice = DataManager.s_Instance.GetVehiclePrice(nPlanetId, nDistrictId, nLevel);
            float nRealPrice = nInitialPrice;
            float fRealDiscount = 0f;

            // 主动技能加成
            Skill skill = MapManager.s_Instance.GetCurDistrict().GetSkill(SkillManager.eSkillType.cost_reduce);
            float fSkillDiscount = 0;
            if (skill.GetStatus() == SkillManager.eSkillStatus.working)
            {
                fSkillDiscount = skill.m_Config.fValue;
                fRealDiscount += fSkillDiscount;
            }
            // end 主动技能加成

            // 科技树加成
            float fScienceDiscount = ScienceTree.s_Instance.GetVehicleBuyDiscount(nPlanetId);
            if (fScienceDiscount != 0)
            {
                fRealDiscount += fScienceDiscount;
            }

            // end 科技树加成


            if (fRealDiscount < -0.9f)
            {
                fRealDiscount = -0.9f;
            }
            nRealPrice = (1 + fRealDiscount) * nInitialPrice;

            int nGain = DataManager.s_Instance.GetCoinGainperRound(nPlanetId, nDistrictId, nLevel);
            float fVehiclerunTimePerRound = DataManager.s_Instance.GetVehicleRunTimePerRound(nLevel );

            UIVehicleCounter counter = null;
            if ( i < m_lstVehicleCounters.Count )
            {
                counter = m_lstVehicleCounters[i];
            }
            else{
                counter = ResourceManager.s_Instance.NewVehicleCounter();
                m_lstVehicleCounters.Add(counter);
            }

            counter._txtInitialPrice.text = "原价：" + nInitialPrice.ToString();
            counter._txtSkillDiscount.text = "技能折扣：" + (fSkillDiscount * 100 ) + "%";
            counter._txtScienceDiscount.text = "科技树折扣：" + (fScienceDiscount * 100) + "%"; ;

            counter.transform.SetParent( _goCounterList.transform );
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            counter.transform.localScale = vecTempScale;

            counter._txtLevel.text = "lv." + nLevel;
            counter._imgAvatar.sprite = ResourceManager.s_Instance.m_aryParkingPlaneSprites[i];

            counter._txtPrice.text = nRealPrice.ToString("f0");

            counter._txtGain.text = "每圈 " + nGain + " 个金币";
            counter._txtSpeed.text = fVehiclerunTimePerRound + "秒绕一圈";

            counter.m_nPrice = (int)nRealPrice;
            counter.m_nVehicleLevel = nLevel;

        } // end for
      
    }

    public void OnClick_OpenTanGeChe()
    {
        _panelTanGeChe.SetActive( true );

        //LoadData( MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
        UpdateCarMallInfo();
    }

    public void OnClick_CloseTanGeChe()
    {
        _panelTanGeChe.SetActive(false);
    }

    public void UpdateCarMallInfo()
    {
        LoadData(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
    }

} // end class
