﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour {

    public static BoxManager s_Instance = null;

    static Vector3 vecTempPos = new Vector3();

    public float m_fGenerateBoxInterval = 15f;

    float m_fTimeElapse = 0f;

    float m_fDropStartPosY = 10f;
    float m_fDropSpeed = 0f;
    public float m_fDropTime = 1f;


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        return;
        GenerateLoop();
	}

    void GenerateLoop()
    {
        m_fTimeElapse += Time.deltaTime;
        if ( m_fTimeElapse < m_fGenerateBoxInterval )
        {
            return;
        }
        m_fTimeElapse = 0;

        Lot lot = Main.s_Instance.GetOneAvailableLotToGenerateBox();
        if ( lot == null )
        {
            return;
        }



        Plane plane = ResourceManager.s_Instance.NewPlane();


        vecTempPos.x = 0f;
        vecTempPos.y = m_fDropStartPosY;
        vecTempPos.z = 0f;
        plane.transform.position = vecTempPos;

        plane.SetLevel(1);
        lot.SetPlane(plane, false);

        vecTempPos = plane.GetPos();
        vecTempPos.x = 0;
        vecTempPos.z = -1;
        plane.SetPos(vecTempPos);

        float fDropDistance = vecTempPos.y;

        plane.ShowTreasureBox(fDropDistance);

        plane.SetPlaneStatus(Main.ePlaneStatus.treasure_box);

      
    }

   

}
