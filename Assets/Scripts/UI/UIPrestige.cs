﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPrestige : MonoBehaviour {

    public Text _txtCurGainTimes;
    public Text _txtNextGainTimes;
    public Text _txtCost;
    public Button _btnDoPrestige;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateInfo()
    {
        Planet planet = MapManager.s_Instance.GetCurPlanet();
        District district = MapManager.s_Instance.GetCurDistrict();
        int nPlanetId = planet.GetId();
        int nDistrictId = district.GetId();
        int nCurPrestigeTimes = district.GetPrestigeTimes();
        int nCurGainTimes = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nCurPrestigeTimes);
        int nNextGainTimes = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nCurPrestigeTimes + 1);
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(nPlanetId, nDistrictId);

        _txtCost.text = nCost.ToString();
        _txtCurGainTimes.text = nCurGainTimes.ToString() + "X";
        _txtNextGainTimes.text = nNextGainTimes.ToString() + "X";


       
    }



} // end class
