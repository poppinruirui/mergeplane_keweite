﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyCounter : MonoBehaviour {

    public Image _imgIcon;
    public Text _txtValue;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetValue( int nValue )
    {
        _txtValue.text = nValue.ToString();
    }

    public void SetIcon( Sprite spr )
    {
        _imgIcon.sprite = spr;
    }

} // end class
