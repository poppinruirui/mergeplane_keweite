﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneUiSkillButton : MonoBehaviour {




    /// <summary>
    /// UI
    /// </summary>
    public TextMesh _txtStatus;
    public TextMesh _txtLeftTime;
    public TextMesh _txtLevel;
    public TextMesh _txtCurValue;
    // end UI


    public SkillManager.eSkillType m_eType = SkillManager.eSkillType.coin_raise;

    public int m_nDuration = 60;
    public int m_nColdDown = 20;

    float m_fColdDownCount = 0;
    float m_DurationCount = 0;

    float m_fTimeElapse = 0f;

    Skill m_BoundSkill = null;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        WorkingLoop();
        ColdDownLoop();

    }

    private void OnMouseUp()
    {
        Skill skill = MapManager.s_Instance.GetCurDistrict().GetSkill(m_eType);

        if (skill.GetStatus() != SkillManager.eSkillStatus.idle)
        {
            return;
        }

        BeginCast();
    }

    void BeginCast()
    {
        m_BoundSkill = MapManager.s_Instance.GetCurDistrict().GetSkill( m_eType );
     //   m_BoundSkill.m_BoundUIBUtton = this;
        m_BoundSkill.SetStartTime( Main.GetSystemTime() );
        if (m_BoundSkill.GetSkillType() == SkillManager.eSkillType.speed_accelerate)
        {
            m_BoundSkill.m_fRealTimeDuration = m_BoundSkill.m_Config.nDuration * (1 + ScienceTree.s_Instance.GetSkillSpeedTimeRaise());
        }
        else
        {
            m_BoundSkill.m_fRealTimeDuration = m_BoundSkill.m_Config.nDuration;
        }

        m_BoundSkill.SetStatus(SkillManager.eSkillStatus.working);

        _txtStatus.text = "使用中...";

  
        switch (m_eType)
        {
            case SkillManager.eSkillType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;

            case SkillManager.eSkillType.speed_accelerate:
                {
                    float fAccelerateValue = m_BoundSkill.m_Config.fValue;
                    Main.s_Instance.AccelerateAll(fAccelerateValue);
                }
                break;
            case SkillManager.eSkillType.cost_reduce:
                {
                    TanGeChe.s_Instance.UpdateCarMallInfo();
                }
                break;
        } // end switch
       


    }

    void WorkingLoop()
    {
        /*
        if (m_BoundSkill.GetStatus() != SkillManager.eSkillStatus.working)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;

        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        _txtLeftTime.text = m_DurationCount.ToString( "f0" );

        m_DurationCount -= 1f;
        if (m_DurationCount <= 0f)
        {
            _txtStatus.text = "ColdDown...";
            m_eStatus = eSkillCastButtonStatus.colddown;
            _txtLeftTime.text = "";

        }
        */
    }

    void ColdDownLoop()
    {
        /*
        if (m_eStatus != eSkillCastButtonStatus.colddown)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        _txtLeftTime.text = m_fColdDownCount.ToString("f0");

        m_fColdDownCount -= 1f;
        if (m_fColdDownCount <= 0f)
        {
            m_eStatus = eSkillCastButtonStatus.idle;
            _txtLeftTime.text = "";
            _txtStatus.text = "";
        }
        */
    }



} // end class
