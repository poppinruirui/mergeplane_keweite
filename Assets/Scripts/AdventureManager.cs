﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureManager : MonoBehaviour {

    public static AdventureManager s_Instance = null;

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtLeftTime;
    public Image _txtProgressbar;
    /// end UI

    bool m_bAdventuring = false;
    int m_nLeftTime = 0;
    int m_nTotalTime = 0;

    UIAdventureCounter m_CurAdventure = null;

    // 探险奖励的类型
    public enum eAdventureProfitType
    {
        coin_raise_item,  // 金币强化道具
        green_cash,       // 绿票
    };

    public GameObject _panelAdventure;
    public GameObject _panelAdventuring;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        AdventuringLoop();

    }

    public void OnClick_OpenAdventurePanel()
    {
        _panelAdventure.SetActive( true );
    }

    public void OnClick_CloseAdventurePanel()
    {
        _panelAdventure.SetActive(false);
    }

    public void BeginAdventure( UIAdventureCounter adventure )
    {
        m_CurAdventure = adventure;
        m_nLeftTime = adventure.m_nDuration;
        m_nTotalTime = adventure.m_nDuration;

        _panelAdventuring.SetActive( true );

        m_bAdventuring = true;
    }

    public void OnClick_CloseAdventuring()
    {
        _panelAdventuring.SetActive(false);
    }

    float m_fTimeElapse = 0;
    void AdventuringLoop()
    {
        if ( !m_bAdventuring)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1)
        {
            return;
        }
        m_fTimeElapse = 0;

        m_nLeftTime -= 1;

        _txtLeftTime.text = m_nLeftTime.ToString() + "秒";

        _txtProgressbar.fillAmount = 1f - (float)m_nLeftTime / m_nTotalTime;

        if (m_nLeftTime <= 0)
        {
            EndAdventure();
        }

    }

    void EndAdventure()
    {
        m_bAdventuring = false;

        _panelAdventuring.SetActive( false );

        // 发奖励
        UIMsgBox.s_Instance.ShowMsg( "探险完成，请查收你的奖励" );

        for (int i = 0; i < m_CurAdventure.m_aryProfitList.Length; i++ )
        {
            UIAdventureProfitCounter profit = m_CurAdventure.m_aryProfitList[i];
            switch( profit.m_eProfitType ) 
            {
                case eAdventureProfitType.coin_raise_item:
                    {
                       
                        UIItemInBag bag_item = ResourceManager.s_Instance.NewUiItem();
                        bag_item.InitBagItemById(profit.m_nItemId);
                        ItemSystem.s_Instance.AddItem(bag_item);

                    }
                    break;

                case eAdventureProfitType.green_cash:
                    {
                        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + profit.m_nValue0);
                    }
                    break;
            } // end switch
        } // end for

    }


} //  end class
